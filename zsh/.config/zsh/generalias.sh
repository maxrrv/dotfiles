#Tar extractions
alias tgz='tar -zxvf' #z: gzip gunzip ungzip / x: extract from archive / v: verbose output / f: use archive file
alias tbz='tar -jxvf' #j: bzip2 / x: extract from archive / v: verbose output / f: use archive file

#remap caps to escape
alias ctoe='setxkbmap -option caps:escape'

#yarn
alias y='yarn'
alias ys='yarn start'
alias yst='yarn storybook'
alias yt='yarn test'
alias ytr='yarn translations:sync'
alias yw='yarn test --watch'
alias yys='yarn; yarn start'
alias yb='yarn build'
alias yl='yarn link'
alias yc='yarn cm'
alias yco='yarn commit'
alias yj='yarn jest'
alias yjd='node --inspect-brk node_modules/.bin/jest --runInBand'

#npm & node
alias n='npm'
alias ni='npm install'
alias ns='npm run start'
alias nst='npm run storybook'
alias nt='npm run test'
alias nw='npm run test --watch'
alias nb='npm run build'
alias nbw='npm run build:watch'
alias nj='npm run jest'
alias np='npm run prettier %'
alias nd='npm run dev'

alias no='node'

#docker
alias d='docker'
alias dc='d compose'
alias dcu='dc up'
alias dcud='dcu -d'
alias dcd='dc down'
alias dl='d logs'
alias dlf='dl -f'
alias dps='d ps'
alias dpsa='dps -a'

#pomodoro
alias p='pomodoro 1500'
alias p30='pomodoro 1800'

alias sortBySize='find . -type f ! -path "./node_modules/*" ! -path "./.git/*" ! -path "*/.gems/*" ! -path "./dist/*" ! -path "./coverage/*" -exec wc -l {} + | sort -rn | less'
alias excuse='curl -s https://pages.cs.wisc.edu/~ballard/bofh/excuses | shuf -n 1'

#check disk size
alias biggie='du -sh *'

#rm
alias rnm='rm -rf node_modules/'
alias wipenm='find . -name \*node_modules -type d -exec rm -rf {} +'

#jump
alias j='jump'
alias m='mark'
alias um='unmark'
alias ms='marks'

#ls
alias ls='ls -G'
alias l='ls -lahG'
alias ll='ls -lhG'

#git
alias g='git'
alias gst='git status'
alias gl='git pull'
alias gp='git push'
alias gc='git commit -v'
alias gco='git checkout'
alias gcb='git checkout -b'
alias gpsup='git push --set-upstream origin $(git_current_branch)'
alias glg='git log -p'
alias gd='git diff'
alias gdm='git diff main... -- ":!package-lock.json"'
alias gdma='git diff master... -- ":!package-lock.json"'
alias gdd='git diff develop... -- ":!package-lock.json"'
alias ga='git add'
alias gau='git add -u'
alias gay='echo "I am happily staging your updated files now :)" && gau'
alias gaa='git add .'
alias gds='git diff --staged'
alias gfa='git fetch --all -p'
alias gm='git merge'
alias gmod='git merge origin/develop'
alias gmom='git merge origin/main'
alias gmoms='git merge origin/master'
alias gb='git branch'
alias gbd='git branch -D'
alias gbda='git branch | xargs git branch -D'
alias lg='lazygit'

#cd
alias c='cd'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias ......='cd ../../../../../'

#vim
alias v='nvim'

#mac
alias heat='sudo powermetrics --samplers smc |grep -i "CPU die temperature"'

# server
alias serve='python3 -m http.server'
alias serve8k='python3 -m http.server 8000'

