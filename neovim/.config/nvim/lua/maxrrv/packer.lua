-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
	-- Packer can manage itself
	use('wbthomason/packer.nvim')

	use({
		'nvim-telescope/telescope.nvim', tag = '0.1.4',
		-- or                            , branch = '0.1.x',
		requires = { {'nvim-lua/plenary.nvim'} }
	})

  use('sbdchd/neoformat')
	use('folke/tokyonight.nvim')
  use('shaunsingh/solarized.nvim')
  use('rebelot/kanagawa.nvim')
  use('NvChad/nvim-colorizer.lua')

  use({ "catppuccin/nvim", as = "catppuccin" })

	use({ 'nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'} })

	use('nvim-treesitter/playground')

	use({
		'ThePrimeagen/harpoon',
		requires = { {'nvim-lua/plenary.nvim'} }
	})

	use('mbbill/undotree')
	use('tpope/vim-fugitive')
  use('tpope/vim-surround')
  use({"ellisonleao/glow.nvim", config = function() require("glow").setup() end})
  use({ "iamcco/markdown-preview.nvim", run = "cd app && npm install", setup = function() vim.g.mkdp_filetypes = { "markdown" } end, ft = { "markdown" }, })

	use({
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v1.x',
		requires = {
			-- LSP Support
			{'neovim/nvim-lspconfig'},             -- Required
			{'williamboman/mason.nvim'},           -- Optional
			{'williamboman/mason-lspconfig.nvim'}, -- Optional

			-- Autocompletion
			{'hrsh7th/nvim-cmp'},         -- Required
			{'hrsh7th/cmp-nvim-lsp'},     -- Required
			{'hrsh7th/cmp-buffer'},       -- Optional
			{'hrsh7th/cmp-path'},         -- Optional
			{'saadparwaiz1/cmp_luasnip'}, -- Optional
			{'hrsh7th/cmp-nvim-lua'},     -- Optional

			-- Snippets
			{'L3MON4D3/LuaSnip'},             -- Required
			{'rafamadriz/friendly-snippets'}, -- Optional
		}
	})
  use('github/copilot.vim')

end)

