vim.g.mapleader = " "
vim.opt.guicursor = ""

vim.opt.number = true
vim.opt.relativenumber = true

-- indentation
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.smartindent = true

-- start scrolling before the cursor hits the borders 
vim.opt.sidescroll = 1
vim.opt.sidescrolloff = 5
vim.opt.scrolloff = 10

vim.opt.colorcolumn = {120}
vim.opt.signcolumn = 'yes'

-- rely on undotree
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- search params
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.incsearch = true

vim.opt.termguicolors = true
vim.opt.updatetime = 50

