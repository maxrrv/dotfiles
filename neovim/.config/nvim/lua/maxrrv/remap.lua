local showCursorline = false

local function toggleCursorline()
  showCursorline = not showCursorline
  vim.opt.cursorline = showCursorline

end

vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)
vim.keymap.set("n", "<leader>v", ":vs<CR>")
vim.keymap.set("n", "<leader>s", ":sp<CR>")
vim.keymap.set("n", "<leader>c", function() toggleCursorline() end)
vim.keymap.set("n", "<leader>dp", ":silent %!dprint fmt %<CR>")
vim.keymap.set("n", "<leader>gp", ":silent %!prettier --stdin-filepath %<CR>")
vim.keymap.set("n", "<leader>h", "<C-w>h")
vim.keymap.set("n", "<leader>j", "<C-w>j")
vim.keymap.set("n", "<leader>k", "<C-w>k")
vim.keymap.set("n", "<leader>l", "<C-w>l")
-- todo think about adding more remaps from primeagen
