local Job = require("plenary.job")
local api = vim.api

local function runNpmBuildOnDirectory (directory)
  local currentdir = tostring(os.getenv("PWD"))
  local fileExists = string.find(currentdir, directory)
  if not(fileExists == nil) then
    Job:new({
      command = "npm",
      args = {"run", "build"},
      on_exit = function()
        print("npm build done")
      end,
    }):start()
  end
end

-- run npm build on save
local npmBuildGroup = api.nvim_create_augroup("NpmBuild", { clear = true })

-- api.nvim_create_autocmd("BufWritePost", {
--   group = npmBuildGroup,
--   pattern = {"*.ts", "*.tsx", "*.css", "*.scss", "*.js", "*.jsx"},
--   callback = function () runNpmBuildOnDirectory("elements") end
-- })

