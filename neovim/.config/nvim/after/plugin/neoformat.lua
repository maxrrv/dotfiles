local api = vim.api

local neoformat = api.nvim_create_augroup("neoformat", { clear = true })

api.nvim_create_autocmd("BufWritePre", {
  group = neoformat,
  pattern = "*",
  command = "undojoin | Neoformat",
})
